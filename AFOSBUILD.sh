rm -rf /opt/ANDRAX/kadimus

make

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Make... PASS!"
else
  # houston we have a problem
  exit 1
fi

rm -rf bin/ src/ Makefile

cp -Rf andraxbin/* /opt/ANDRAX/bin
rm -rf andraxbin

cp -Rf $(pwd) /opt/ANDRAX/kadimus

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
